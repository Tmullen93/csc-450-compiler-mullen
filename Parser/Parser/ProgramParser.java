package ParserPart2;
import java.awt.List;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import Parser.EquationScanner;
import Parser.EquationToken;
import ProjectPart3.SymbolTable;
import Syntax.AssignmentStatementNode;
import Syntax.CompoundStatementNode;
import Syntax.DeclarationsNode;
import Syntax.ExpressionNode;
import Syntax.IfStatementNode;
import Syntax.OperationNode;
import Syntax.ProgramNode;
import Syntax.StatementNode;
import Syntax.SubProgramDeclarationsNode;
import Syntax.ValueNode;
import Syntax.VariableNode;
import Syntax.WhileStatementNode;

public class ProgramParser {
	//Add a scanner before starting the parser.
	private EquationScanner scanner;
	private EquationToken currentToken;
	
	 public ProgramParser( String filename) {
	        File input = new File( filename);
	        scanner = new EquationScanner( input);
	        
	        // Load in the first token as the lookahead token:
	        scanner.nextToken();
	        currentToken = scanner.getToken();
	    }
	 /*
	  * Print program to the console, the program should begin with
	  * the word program followed by the program's ID.
	  * 
	  * After this the declarations, subprgram declarations and compound statement methods are called.
	  */
	 public ProgramNode program() {
	        System.out.println("program");
	        match( EquationToken.PROGRAM);
	        String valuePN = scanner.getLexeme();
	        match( EquationToken.ID);
	        match( EquationToken.SEMI_COLON);
	        DeclarationsNode dn = declarations();
	        SubProgramDeclarationsNode spdn = subprogram_declarations();
	        CompoundStatementNode csn = compound_statement(); 
	        System.out.println("program after compound-statement");
	        match( EquationToken.DOT);
			ProgramNode pn = new ProgramNode(valuePN);
			pn.setVariables(dn);
			pn.setFunctions(spdn);
			pn.setMain(csn);
			return pn;
	    }
	/*
	 * Get all of the declarations and return them
	 * as an ArrayList.
	 */
	public ArrayList<VariableNode> identifier_list(ArrayList<VariableNode> alvn){
		 System.out.println("identifier_list");
		 String valueVN = scanner.getLexeme();
		 match(EquationToken.ID);
		 VariableNode vn = new VariableNode(valueVN);
		 alvn.add(vn);
		 if(currentToken == EquationToken.COMMA){
			 match(EquationToken.COMMA);
			 alvn = identifier_list(alvn);
			 }
		 return alvn;
		 }
	 /*
	  * Create a declarations node and go to
	  * identifier_list.
	  */
	 public DeclarationsNode declarations() {
	        System.out.println("declarations");
	        DeclarationsNode dn = new DeclarationsNode();
	        ArrayList<VariableNode> alvn = new ArrayList<VariableNode>();
	        if(currentToken == EquationToken.VARIABLE){
	        	match(EquationToken.VARIABLE);
	        	alvn = identifier_list(alvn);
	        	dn.addAll(alvn);
	        	match(EquationToken.COLON);
	        	type();
	        	match(EquationToken.SEMI_COLON);
	        	declarations();
	        }
			return dn;
	    }
	 
	 public void type(){
		 System.out.println("type");
		 if(currentToken == EquationToken.ARRAY){
			 match(EquationToken.ARRAY);
			 match(EquationToken.LEFT_BRACE);
			 match(EquationToken.NUM);
			 match(EquationToken.COLON);
			 match(EquationToken.NUM);
			 match(EquationToken.RIGHT_BRACE);
			 match(EquationToken.OF);
			 standard_type();
		 }
		 else{
			 standard_type();
		 }
	 }
	 
	 public ExpressionNode standard_type(){
		 System.out.println("standard_type");
		 ExpressionNode en = null;
		 if(currentToken == EquationToken.INTEGER){
			 String ValueVN = scanner.getLexeme();
			 match(EquationToken.INTEGER);
			 en = new ValueNode(ValueVN);
		 }
		 else if(currentToken == EquationToken.REAL){
			 match(EquationToken.REAL);
		 }
		 else{
			 System.out.println("Error at standard_type");
			 error();
		 }
		 return en;
	 }
	 
	 public SubProgramDeclarationsNode subprogram_declarations(){
		 System.out.println("subprogram-declarations");
		 SubProgramDeclarationsNode spdn = new SubProgramDeclarationsNode();
		 ArrayList<SubProgramDeclarationsNode> alspdn = new ArrayList<SubProgramDeclarationsNode>();
		 if(currentToken == EquationToken.FUNCTION ||currentToken == EquationToken.PROCEDURE){
		 	alspdn = subprogram_declaration(alspdn);
		 	match(EquationToken.SEMI_COLON);
		 	subprogram_declarations();
		 }
		return spdn;
	 }
	 
	 public ArrayList<SubProgramDeclarationsNode> subprogram_declaration(ArrayList<SubProgramDeclarationsNode> alspdn){
		 System.out.println("subprogram_declaration");
		 subprogram_head();
		 declarations();
		 subprogram_declarations();
		 compound_statement();
		 System.out.println("subprogram_declaration, after compound statement");
		 return alspdn;
	 }
	 
	 public void subprogram_head(){
		 System.out.println("subprogram_head");
		 if(currentToken == EquationToken.FUNCTION){
			 match(EquationToken.FUNCTION);
			 match(EquationToken.ID);
			 arguments();
			 match(EquationToken.COLON);
			 standard_type();
			 match(EquationToken.SEMI_COLON);
		 }
		 
		 else if(currentToken == EquationToken.PROCEDURE){
			 match(EquationToken.PROCEDURE);
			 match(EquationToken.ID);
			 arguments();
			 match(EquationToken.SEMI_COLON);
		 }
		 else{
			 System.out.println("error at subprogram_head");
			 error();
		 }
	 }
	 
	 public void arguments(){
		 System.out.println("arguments");
		 if(currentToken == EquationToken.LEFT_PAREN){
			 match(EquationToken.LEFT_PAREN);
			 parameter_list();
			 match(EquationToken.RIGHT_PAREN);
		 }
	 }
	 
	 public void parameter_list(){
		 System.out.println("parameter_list");
		 ArrayList<VariableNode> alvn = new ArrayList<VariableNode>();
		 identifier_list(alvn);
		 match(EquationToken.COLON);
		 type();
		 
		 if(currentToken == EquationToken.SEMI_COLON){
			 match(EquationToken.SEMI_COLON);
			 parameter_list();
		 }
	 }
	 
	 public CompoundStatementNode compound_statement() {
	        System.out.println("compound_statement");
	        CompoundStatementNode csn = new CompoundStatementNode();
	        ArrayList<StatementNode> alsn = new ArrayList<StatementNode>();
	        match( EquationToken.BEGIN);
	        optional_statements(alsn);
	        match( EquationToken.END);
	        System.out.println("compound_statement end");
	        csn.addAll(alsn);
			return csn;
	    }
	 
	 public void optional_statements(ArrayList<StatementNode>alsn) {
	        System.out.println("optional_statements");
	        StatementNode sn = null;
	        if(currentToken == EquationToken.ID){
	        	statement_list(alsn);
	        }
	        else if(currentToken == EquationToken.BEGIN){
	        	statement_list(alsn);
	        }
	        else if(currentToken == EquationToken.IF){
	        	statement_list(alsn);
	        }
	        else if(currentToken == EquationToken.WHILE){
	        	statement_list(alsn);
	        }
	    }
	 
	 public void statement_list(ArrayList<StatementNode>alsn){
		 System.out.println("statement_list");
		 StatementNode sn = null;
		 sn = statement();
		 alsn.add(sn);
		 if(currentToken == EquationToken.SEMI_COLON){
			 match(EquationToken.SEMI_COLON);
			 statement_list(alsn);
		 }
		 
	 }
	 
	 public StatementNode statement(){
		 System.out.println("statement");
		 StatementNode sn = null;
		 if(currentToken == EquationToken.ID){
			 AssignmentStatementNode asn = new AssignmentStatementNode();
			 VariableNode vn = variable();
			 asn.setValue(vn);
			 match(EquationToken.ASSIGNOP);
			 ExpressionNode en = expression();
			 asn.setFunctions(en);
			 sn = asn;
			 return sn;
		 }
		 else if(currentToken == EquationToken.BEGIN){
			 sn = compound_statement();
		 }
		 else if(currentToken == EquationToken.IF){
			 IfStatementNode isn = new IfStatementNode();
			 match(EquationToken.IF);
			 ExpressionNode en = expression();
			 isn.setTest(en);
			 match(EquationToken.THEN);
			 sn = statement();
			 isn.setdoThen(sn);
			 match(EquationToken.ELSE);
			 sn = statement();
			 isn.setdoElse(sn);
			 sn = isn;
			 return sn;
		 }
		 else if(currentToken == EquationToken.WHILE){
			 WhileStatementNode wsn = new WhileStatementNode();
			 match(EquationToken.WHILE);
			 ExpressionNode en = expression();
			 wsn.setTest(en);
			 match(EquationToken.DO);
			 sn = statement();
			 wsn.setdoWhile(sn);
			 sn = wsn;
			 return sn;
		 }
		 else{
			 System.out.println("error at statement");
			 error();
		 }
		 return sn;
	 }
	 
	 public VariableNode variable(){
		 System.out.println("variable");
		 String Namevn = scanner.getLexeme();
		 match(EquationToken.ID);
		 VariableNode vn = new VariableNode(Namevn);
		 if(currentToken == EquationToken.LEFT_BRACE){
			 match(EquationToken.LEFT_BRACE);
			 expression();
			 match(EquationToken.RIGHT_BRACE);
		 }
		 return vn;
	 }
	 
	 public ExpressionNode procedure_statement(){
		 System.out.println("procedure_statement");
		 ExpressionNode en = null;
		 match(EquationToken.ID);
		 if(currentToken == EquationToken.LEFT_PAREN){
			 match(EquationToken.LEFT_PAREN);
			 en = expression_list();
			 match(EquationToken.RIGHT_PAREN);
		 }
		 return en;
	 }
	 
	 public ExpressionNode expression_list(){
		 System.out.println("expression_list");
		 expression();
		 if(currentToken == EquationToken.COMMA){
			 match(EquationToken.COMMA);
			 expression_list();
		 }
		return null;
	 }
	 
	 public ExpressionNode expression(){
		 System.out.println("expression");
		 ExpressionNode en = null;
		 en = simple_expression();
		 if(currentToken == EquationToken.EQUALS){
			 ExpressionNode left = en;
			 OperationNode on = new OperationNode(scanner.getToken());
			 match(EquationToken.EQUALS);
			 ExpressionNode Right = simple_expression();
			 on.setLeft(left);
			 on.setRight(Right);
			 en = on;
		 }
		 
		 else if(currentToken == EquationToken.LESS_THAN){
			 ExpressionNode left = en;
			 OperationNode on = new OperationNode(scanner.getToken());
			 match(EquationToken.LESS_THAN);
			 ExpressionNode Right = simple_expression();
			 on.setLeft(left);
			 on.setRight(Right);
			 en = on;
		 }
		 
		 else if(currentToken == EquationToken.GREATER_THAN){
			 ExpressionNode left = en;
			 OperationNode on = new OperationNode(scanner.getToken());
			 match(EquationToken.GREATER_THAN);
			 ExpressionNode Right = simple_expression();
			 on.setLeft(left);
			 on.setRight(Right);
			 en = on;
		 }
		 
		 else if(currentToken == EquationToken.LESS_THAN_OR_EQUAL){
			 ExpressionNode left = en;
			 OperationNode on = new OperationNode(scanner.getToken());
			 match(EquationToken.LESS_THAN_OR_EQUAL);
			 ExpressionNode Right = simple_expression();
			 
		 }
		 
		 else if(currentToken == EquationToken.GREATER_THAN_OR_EQUAL){
			 ExpressionNode left = en;
			 OperationNode on = new OperationNode(scanner.getToken());
			 match(EquationToken.GREATER_THAN_OR_EQUAL);
			 ExpressionNode Right = simple_expression();
			 on.setLeft(left);
			 on.setRight(Right);
			 en = on;
		 }
		return en;
	 }
	 
	 public ExpressionNode simple_expression(){
		 System.out.println("simple_expression");
		 ExpressionNode en = null;
		 en = term();
		 simple_part();
		 if(currentToken == EquationToken.ADDOP){
	         sign();
	         term();
		 }
		 if(currentToken == EquationToken.MINUS){
	         sign();
	         term();
		 }
		 return en;
	 }
	 
	 public ExpressionNode simple_part(){
		 System.out.println("simple_part");
		 ExpressionNode en = null;
		 if(currentToken == EquationToken.ADDOP){
			 match(EquationToken.ADDOP);
			 term();
			 simple_part();
		 }
		 
		 else if(currentToken == EquationToken.MINUS){
			 match(EquationToken.MINUS);
			 term();
			 simple_part();
		 }
		 return en;
	 }
	 
	 public ExpressionNode term(){
		 System.out.println("term");
		 ExpressionNode en = factor();
		 if(currentToken == EquationToken.MULOP){
			 ExpressionNode left = en;
	         OperationNode on = term_part();
	         ExpressionNode right = factor();
	         on.setLeft(left);
	         on.setRight(right);
	         en = on;
		 }
		 
		 if(currentToken == EquationToken.DIVIDE){
			 ExpressionNode left = en;
			 OperationNode on = term_part();
	         ExpressionNode right = factor();
	         on.setLeft(left);
	         on.setRight(right);
	         en = on;
		 }
		 return en;
	 }
	 
	 public OperationNode term_part(){
		 System.out.println("term_part");
		 OperationNode on = null;
		 if(currentToken == EquationToken.MULOP){
			 EquationToken Operationon = scanner.getToken();
			 match(EquationToken.MULOP);
			 on = new OperationNode(Operationon);
			 //factor();
			 term_part();
		 }
		 
		 else if(currentToken == EquationToken.DIVIDE){
			 EquationToken Operationon = scanner.getToken();
			 match(EquationToken.DIVIDE);
			 on = new OperationNode(Operationon);
			 //factor();
			 term_part();
		 }
		 return on;
	 }
	 
	 public ExpressionNode factor(){
		 System.out.println("factor");
		 ExpressionNode en = null;
		 if(currentToken == EquationToken.ID){
			 String valueVN = scanner.getLexeme();
			 match(EquationToken.ID);
			 en = new VariableNode(valueVN);
			 if(currentToken == EquationToken.LEFT_BRACE){
				 match(EquationToken.LEFT_BRACE);
				 match(EquationToken.RIGHT_BRACE);
			 }
			 else if(currentToken == EquationToken.LEFT_PAREN){
				 match(EquationToken.LEFT_PAREN);
				 en =  expression_list();
				 match(EquationToken.RIGHT_PAREN);
			 }
		 }
		 else if(currentToken == EquationToken.NUM){
			 String value = scanner.getLexeme();
			 match(EquationToken.NUM);
			 en = new ValueNode(value);
		 }
		 else if(currentToken == EquationToken.LEFT_PAREN){
			 match(EquationToken.LEFT_PAREN);
			 en = expression_list();
			 match(EquationToken.RIGHT_PAREN);
		 }
		 else if(currentToken == EquationToken.NOT){
			 match(EquationToken.NOT);
			 en = factor();
		 }
		return en;
	 }
	 
	 public OperationNode sign(){
		 System.out.println("sign");
		 OperationNode on = new OperationNode(null);
		 if(currentToken == EquationToken.ADDOP){
			 EquationToken Operationon = scanner.getToken();
			 match(EquationToken.ADDOP);
			 on = new OperationNode(Operationon);
		 }
		 else if(currentToken == EquationToken.MINUS){
			 EquationToken Operationon = scanner.getToken();
			 match(EquationToken.MINUS);
			 on = new OperationNode(Operationon);
		 }
		 else{
			 System.out.println("error at sign");
			 error();
		 }
		return on;
	 }
	
	public void match( EquationToken expectedToken) {
	        System.out.println("match " + expectedToken + " with current " + currentToken + ":" + scanner.getLexeme());
	        if( currentToken == expectedToken) {
	            boolean scanResult = scanner.nextToken();
	            if( scanResult) {
	                currentToken = scanner.getToken();
	            }
	            else {
	                System.out.println("No Token Available");
	                String lexeme = scanner.getLexeme();
	                if( lexeme == null) {
	                    System.out.println("End of file");
	                }
	                else {
	                    System.out.println("Scanner barfed on " + lexeme);
	                }
	            }
	            //System.out.println("   next token is now " + currentToken);
	            //System.out.println("   next attri is now " + scanner.getAttribute());
	        }
	        else {
	        	System.out.println("error at match");
	            error();  // We don't match!
	        }
	    }
	 
	 public void error() {
	        System.exit( 1);
	    }
}
