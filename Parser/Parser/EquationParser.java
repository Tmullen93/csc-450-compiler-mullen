package ParserPart2;
import java.io.File;
import Parser.EquationScanner;
import Parser.EquationToken;
import Parser.NextTokenReturnValue;
import Parser.Lookup_Table;

public class EquationParser {
	
	 private EquationScanner scanner;
	 private EquationToken currentToken;
	 
	 public EquationParser( String filename) {
		 	/*
		 	 * Prepares the parser by creating a new scanner and
		 	 * loading the lookup_table.
		 	 */
	        File input = new File( filename);
	        Lookup_Table symbols = new Lookup_Table();
	        scanner = new EquationScanner(input);
	        
	        // Load in the first token as the lookahead token:
	        scanner.nextToken();
	        currentToken = scanner.getToken();
	    }
	 
	 public void exp() {
	        //call term and match while - or +
	        term();
	        while( currentToken == EquationToken.MINUS ||
	                currentToken == EquationToken.ADDOP) {
	            match( currentToken);
	            term();
	        }
	    }
	 
	 public void term() {
	        //call factor and match while / or *
	        factor();
	        while( currentToken == EquationToken.DIVIDE ||
	                currentToken == EquationToken.MULOP) {
	            match( currentToken);
	            factor();
	        }
	    }
	 
	 public void factor() {
	       /*
	        * Match any numbers to the number token type and
	        * make sure all left_paren's are eventually matched up with a right paren.
	        */
	        if( currentToken == EquationToken.LEFT_PAREN) {
	            match( EquationToken.LEFT_PAREN);
	            exp();
	            match( EquationToken.RIGHT_PAREN);
	        }
	        else if( currentToken == EquationToken.NUM) {
	            match( EquationToken.NUM);
	        }
	        else {
	            System.out.println("Error in Factor. Saw " + currentToken);
	            error();
	        }
	    }
	 
	 public void match( EquationToken expectedToken) {
	        if( currentToken == expectedToken) {
	            boolean scanResult = scanner.nextToken();
	            
	            //After a token is found call the getToken function.
	            if(scanResult == true){
	            	currentToken = scanner.getToken();
	            }
	        }
	        
	        else{
	        	error();
	        }
	 }
	 //Exit if there is an error.
	 public void error() {
	        System.out.println("Error");
	        System.exit( 1);
	    }
}
