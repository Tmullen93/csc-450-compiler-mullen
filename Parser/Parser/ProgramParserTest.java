package ParserPart2;
import Syntax.ProgramNode;

public class ProgramParserTest {

	public static void main(String[] args){
		/*
		 * Creates a new parser and with the test file as input
		 *
		 * To test the program parser, the program function is called
		 * and if the parse is successful a message will be printed out
		 * if it is not, the program will terminate or fail before then.
		 */
		String filename = "C:/test/BitCoins.txt";
        ProgramParser pp = new ProgramParser( filename);
        ProgramNode pn = pp.program();
        System.out.println("Parse of " + filename + " Successful");
        System.out.println("This is the tree: ");
        System.out.println(pn.indentedToString(0));
	}
}
