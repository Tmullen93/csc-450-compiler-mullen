package ParserPart2;

public class EquationParserTest {
	
	public static void main(String[] args) {
		/*
		 * Creates a new parser and with the test file as input
		 * 
		 * To test the equation parser, the exp function is called
		 * and if the parse is successful a message will be printed out
		 * if it is not, the program will terminate or fail before then.
		 */
        String filename = "C:/Test/basicPascal.txt";
        EquationParser ep = new EquationParser( filename);
        ep.exp();
        System.out.println("Parse of " + filename + " Successful");
    }
}
