package Syntax;

public class VariableNode extends ExpressionNode{
	
	private String name;
	
	public VariableNode(String s) {
		this.name = s;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
    public String toString() {
        return(name);
    }
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "VariableNode: " + this.name + "\n";
        return answer;
	}
}
