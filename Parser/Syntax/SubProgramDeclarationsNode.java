package Syntax;
import java.util.ArrayList;

public class SubProgramDeclarationsNode extends SyntaxTreeNode{
	
	private ArrayList<SubProgramDeclarationsNode> procs = new ArrayList<SubProgramDeclarationsNode>();
	
	public void add(SubProgramDeclarationsNode v){
		this.procs.add(v);
	}
	
	public void addAll(ArrayList<SubProgramDeclarationsNode> spdn){
		this.procs.addAll(spdn);
	}
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "SubProgramDeclarationsNode:  \n";
        for(SubProgramDeclarationsNode bob: procs){
        	answer += bob.indentedToString(level + 1);
        }
        return answer;
	}
}