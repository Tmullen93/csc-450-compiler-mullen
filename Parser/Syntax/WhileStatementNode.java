package Syntax;

public class WhileStatementNode extends StatementNode{
	
	private ExpressionNode test;
	private StatementNode doWhile;
	
	public ExpressionNode getTest(){
		return test;
	}
	
	public void setTest(ExpressionNode test){
		this.test = test;
	}
	
	public StatementNode getdoWhile(){
		return doWhile;
	}
	
	public void setdoWhile(StatementNode doWhile){
		this.doWhile = doWhile;
	}
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
		answer += "WhileStatementNode: " + " \n";
		answer += this.test.indentedToString(level + 1);
		answer += doWhile.indentedToString(level + 1);
		return ( answer);
	}
}
