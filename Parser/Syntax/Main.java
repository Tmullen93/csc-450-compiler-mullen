package Syntax;
import Parser.EquationToken;

public class Main {
	
	public static void main(String[] args){
		
		ProgramNode pn = new ProgramNode("sample");
		DeclarationsNode dn = new DeclarationsNode();
		pn.setVariables(dn);
		
		VariableNode v1 = new VariableNode("dollars");
		VariableNode v2 = new VariableNode("yen");
		VariableNode v3 = new VariableNode("bitcoins");
		dn.add(v1);
		dn.add(v2);
		dn.add(v3);
		
		SubProgramDeclarationsNode SDN = new SubProgramDeclarationsNode();
		pn.setFunctions(SDN);
		
		CompoundStatementNode CSN = new CompoundStatementNode();
		pn.setMain(CSN);
		
		AssignmentStatementNode ASN1 = new AssignmentStatementNode();
		
		VariableNode v4 = new VariableNode("dollars");
		ValueNode vlu1 = new ValueNode("1000000");
		ASN1.setValue(v4);
		ASN1.setFunctions(vlu1);
		CSN.add(ASN1);
		
		AssignmentStatementNode ASN2 = new AssignmentStatementNode();
		
		VariableNode v5 = new VariableNode("yen");
		ASN2.setValue(v5);
		
		OperationNode O1 = new OperationNode(EquationToken.MULOP);
		
		VariableNode v6 = new VariableNode("dollars");
		ValueNode vlu2 = new ValueNode("102");
		O1.setLeft(v6);
		O1.setRight(vlu2);
		ASN2.setFunctions(O1);
		CSN.add(ASN2);
		
		AssignmentStatementNode ASN3 = new AssignmentStatementNode();
		
		VariableNode v7 = new VariableNode("bitcoins");
		ASN3.setValue(v7);
		
		OperationNode O2 = new OperationNode(EquationToken.DIVIDE);
		
		VariableNode v8 = new VariableNode("dollars");
		ValueNode vlu3 = new ValueNode("400");
		O2.setLeft(v8);
		O2.setRight(vlu3);
		ASN3.setFunctions(O2);
		CSN.add(ASN3);
		
		String result = pn.indentedToString( 0);
		System.out.println(result);
	}
}