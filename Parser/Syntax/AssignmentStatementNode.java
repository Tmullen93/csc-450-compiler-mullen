package Syntax;
import Parser.EquationToken;

public class AssignmentStatementNode extends StatementNode{
	
	private VariableNode value;
	private ExpressionNode expression;
	
	public ExpressionNode getExpression() {
		return expression;
	}

	public void setFunctions(ExpressionNode expression) {
		this.expression = expression;
	}
	
	public VariableNode getValue() {
		return value;
	}

	public void setValue(VariableNode value) {
		this.value = value;
	}
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "AssignmentStatementNode: \n";
        answer += value.indentedToString(level + 1);
        answer += expression.indentedToString(level + 1);
        return( answer);
	}

}
