package Syntax;
import java.util.ArrayList;

public abstract class StatementNode extends SyntaxTreeNode{
	
	
	public String indentedToString( int level) {
        String answer = "";
        if( level > 0) {
            answer = "|-- ";
        }
        for( int indent = 1; indent < level; indent++) answer += "--- ";
        return( answer);
    }
}
