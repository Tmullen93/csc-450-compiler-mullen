package Syntax;
import Parser.EquationToken;

public abstract class ExpressionNode extends SyntaxTreeNode{
	
	public String indentedToString( int level) {
        String answer = "";
        if( level > 0) {
            answer = "|-- ";
        }
        for( int indent = 1; indent < level; indent++) answer += "--- ";
        return( answer);
    }
}	