package Syntax;

public class ValueNode extends ExpressionNode{
	
	private String attribute;
	
	public ValueNode(String s) {
		this.attribute = s;
	}

	public String getAttribute(){
		return attribute;
	}
	
	public void setAttribute(String attribute){
		this.attribute = attribute;
	}
	
	@Override
    public String toString() {
        return( attribute);
    }
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "ValueNode: " + this.attribute + "\n";
        return answer;
	}
}
