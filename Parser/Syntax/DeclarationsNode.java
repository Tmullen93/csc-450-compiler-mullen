package Syntax;
import java.util.ArrayList;

public class DeclarationsNode extends SyntaxTreeNode{
	
	private ArrayList<VariableNode> vars = new ArrayList<VariableNode>();

	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "DeclarationsNode:  \n";
        for(VariableNode fred: vars){
        	answer += fred.indentedToString(level + 1);
        }
        return( answer);
	}
	public void add(VariableNode vn){
		this.vars.add(vn);
	}
	
	public void addAll(ArrayList<VariableNode> vn){
		this.vars.addAll(vn);
	}
}
