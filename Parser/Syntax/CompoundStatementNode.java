package Syntax;
import java.util.ArrayList;

public class CompoundStatementNode extends StatementNode{
	
	private ArrayList<StatementNode> statements = new ArrayList<StatementNode>();
	
	public void add(StatementNode v){
		this.statements.add(v);
	}
	
	public void addAll(ArrayList<StatementNode> sn){
		this.statements.addAll(sn);
	}
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "CompoundStatementNode: \n";
        for(StatementNode foo: statements){
        	answer += foo.indentedToString(level + 1);
        }
        return( answer);
	}
}
