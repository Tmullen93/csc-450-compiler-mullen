package Syntax;
import Parser.EquationToken;

public class OperationNode extends ExpressionNode{
	
	private EquationToken operation;
	private ExpressionNode left;
	private ExpressionNode right;
	
	 public OperationNode (EquationToken op) {
	        this.operation = op;
	    }
	
	public EquationToken getOperation() {
		return this.operation;
	}

	public void setOperation (EquationToken op) {
		this.operation = op;
	}
	
	public ExpressionNode getLeft() {
		return this.left;
	}

	public void setLeft (ExpressionNode left) {
		this.left = left;
	}
	
	public ExpressionNode getRight() {
		return this.right;
	}

	public void setRight (ExpressionNode right) {
		this.right = right;
	}

	@Override
    public String toString() {
        return operation.toString();
    }
	
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "OperationNode: " + this.operation + "\n";
        answer += left.indentedToString(level + 1);
        answer += right.indentedToString(level + 1);
        return( answer);
	}
}