package Syntax;

public class ProgramNode extends SyntaxTreeNode{
	
	private static String name;
	private DeclarationsNode variables;
	private CompoundStatementNode main;
	private SubProgramDeclarationsNode functions;
	
	public ProgramNode(String name){
		this.name = name;
	}
	
	public DeclarationsNode getVariables() {
		return variables;
	}

	public void setVariables(DeclarationsNode variables) {
		this.variables = variables;
	}

	public CompoundStatementNode getMain() {
		return main;
	}

	public void setMain(CompoundStatementNode main) {
		this.main = main;
	}

	public SubProgramDeclarationsNode getFunctions() {
		return functions;
	}

	public void setFunctions(SubProgramDeclarationsNode functions) {
		this.functions = functions;
	}

	public static String getName() {
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
        answer += "ProgramNode: " + this.name + "\n";
        answer += variables.indentedToString(level + 1);
        answer += functions.indentedToString(level + 1);
        answer += main.indentedToString(level + 1);
        return( answer);
	}
}
