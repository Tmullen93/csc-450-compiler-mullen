package Syntax;

public class IfStatementNode extends StatementNode {
	
	private ExpressionNode test;
	private StatementNode doThen;
	private StatementNode doElse;
	
	public ExpressionNode getTest(){
		return test;
	}
	
	public void setTest(ExpressionNode test){
		this.test = test;
	}
	
	public StatementNode getdoThen(){
		return doThen;
	}
	
	public void setdoThen(StatementNode doThen){
		this.doThen = doThen;
	}
	
	public StatementNode getdoElse(){
		return doElse;
	}
	
	public void setdoElse(StatementNode doElse){
		this.doElse = doElse;
	}
	@Override
	public String indentedToString(int level){
		String answer = super.indentedToString(level);
		answer += "IfStatementNode: " + " \n";
		answer += this.test.indentedToString(level + 1);
		answer += doThen.indentedToString(level + 1);
		answer += doElse.indentedToString(level + 1);
		return ( answer);
	}
}
