package Parser;

/*
 * A list of all of the symbols and keywords 
 * that can be made into tokens.
 */

	public enum EquationToken {
	    VARIABLE,
	    ARRAY,
	    NUM,
	    OF,
	    OR,
	    INTEGER,
	    REAL,
	    FUNCTION,
	    PROCEDURE,
	    BEGIN,
	    END,
	    IF,
	    THEN,
	    ELSE,
	    WHILE,
	    DO,
	    NOT,
	    DIV,
	    MOD,
	    AND,
	    LEFT_BRACKET,
	    RIGHT_BRACKET,
	    EQUALS,
	    LESS_THAN,
	    GREATER_THAN,
	    LESS_THAN_OR_EQUAL,
	    GREATER_THAN_OR_EQUAL,
	    ADDOP,
	    MINUS,
	    MULOP,
	    DIVIDE,
	    DOT,
	    ASSIGNOP,
	    LEFT_PAREN,
	    RIGHT_PAREN,
	    LEFT_BRACE,
	    RIGHT_BRACE,
	    PROGRAM,
	    ID,
	    SEMI_COLON,
	    COLON,
	    COMMA
	}
