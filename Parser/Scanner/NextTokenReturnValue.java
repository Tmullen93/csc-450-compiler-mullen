package Parser;

/*
 * This is a list of the return values when tokens are found.
 */

public enum NextTokenReturnValue {
	TOKEN_AVAILABLE,
	TOKEN_NOT_AVAILABLE,
	INPUT_COMPLETE
}