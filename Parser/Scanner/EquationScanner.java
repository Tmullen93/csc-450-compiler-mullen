package Parser;

import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PushbackReader;

import Parser.Lookup_Table;
import Parser.EquationToken;
/*
 * This part of the project goes through the text file
 * and converts it into a series of tokens.
 * 
 * The tokens are then matched up with the look-up table.
 */
public class EquationScanner {
	/*
	 * Matches each of the states with a number.
	 * being 100 or greater means to stop adding characters to the token.
	 */
	private final int START = 0;
    private final int IN_ID_OR_KEYWORD = 1;
    private final int IN_NUMBER = 2;
    private final int ERROR = 100;
    private final int ID_COMPLETE = 101;
    private final int SYMBOL_COMPLETE = 102;
    private final int SHORT_SYMBOL_COMPLETE = 103;
    private final int NUMBER_COMPLETE = 104;
    
    private EquationToken type;
    private String lexeme;
    private PushbackReader input;   
    private Lookup_Table lookup = new Lookup_Table();
    
    public EquationScanner( File inputFile) {
    	/*
    	 * Create a new file reader
    	 * and throw an exception if the file could not be found.
    	 */
        
        FileReader fr = null;
        try {
            fr = new FileReader(inputFile);
        }
        catch( FileNotFoundException fnfe) {
            System.out.println("Can't find file " + inputFile + ".");
            System.exit(1);
        }
        this.input = new PushbackReader( fr);
    }
    
    
    public boolean nextToken() {
    	/*
    	 * Start is state 0 and a empty lexeme.
    	 */
        int stateNumber = 0;
        String currentLexeme = "";
        int currentCharacter = 0;
        
        while( stateNumber < ERROR) {
            try {
                currentCharacter = input.read();
            }
            catch( IOException ioe) {
            }
            switch( stateNumber) {
            /*
             * start at state zero,
             * with lexeme and type set to null.
             */
                case START:
                    if( currentCharacter == -1) {
                        // at the end of file, set both lexeme and token type to null
                        this.lexeme = null;
                        this.type = null;
                        return(false);
                    }
                    else if( Character.isLetter( currentCharacter)) {
                        stateNumber = IN_ID_OR_KEYWORD;
                        currentLexeme += (char)currentCharacter;
                    }
                    /*
                     * Make the lexeme a number if it starts with a digit.
                     */
                    else if( Character.isDigit( currentCharacter)){
                    	stateNumber = IN_NUMBER;
                    	currentLexeme += (char)currentCharacter;
                    }
                    //Ignore any whitespace.
                    else if ( Character.isWhitespace( currentCharacter)) {
                        
                    }
                    else if( currentCharacter == '+' ||
                             currentCharacter == '-' ||
                             currentCharacter == '*' ||
                    		 currentCharacter == '/' ||
                    		 currentCharacter == '=' ||
                    		 currentCharacter == '.' ||
                    		 currentCharacter == ';' ||
                    		 currentCharacter == ',' ||
                    		 currentCharacter == '(' ||
                    		 currentCharacter == ')' ||
                    		 currentCharacter == '[' ||
                    		 currentCharacter == ']' ) {
                        stateNumber = SYMBOL_COMPLETE;
                        currentLexeme += (char)currentCharacter;
                    }
                    /*
                     * Since > and < are sperate symbols 
                     * from >= and <=, they are sent to a different state.
                     * 
                     * This is because >= and <= are the only symbols that are more than one character.
                     */
                    else if( currentCharacter == '>' ||
                    		 currentCharacter == '<' ||
                    		 currentCharacter == ':') {
                        stateNumber = 3;
                        currentLexeme += (char)currentCharacter;
                    }
                    /*
                     * brackets are meant to notate comments.
                     */                
                    else if( currentCharacter == '{') {
                    	this.type = EquationToken.LEFT_BRACKET;
                        stateNumber = 4;
                    }
                    /*
                     * := is meant to mean assign.
                     */
                    else if( currentCharacter == ':'){
                    	stateNumber = 5;
                    }
                    /*
                     * Go to error state if any other type of
                     * character is listed.
                     */
                    else {
                        currentLexeme += (char)currentCharacter;
                        stateNumber = ERROR;
                    }
                    break;
                case IN_ID_OR_KEYWORD:
                    if( currentCharacter == -1) {
                        stateNumber = ID_COMPLETE;                        
                    }
                    /*
                     * continue adding to current lexeme if
                     * the next character is a digit or number.
                     */
                    else if( Character.isLetterOrDigit( currentCharacter)) {
                        currentLexeme += (char)currentCharacter;                        
                    }
                    else {
                        try {
                            input.unread( currentCharacter);
                        }
                        catch( IOException ioe){
                           
                        }
                        stateNumber = ID_COMPLETE;
                    }
                    break;
                    
                case IN_NUMBER:
                	if( currentCharacter == -1){
                	  stateNumber = NUMBER_COMPLETE;	
                	}
                	//Keep adding to the lexeme if the next character is a number.
                	
                	else if(Character.isDigit( currentCharacter)){
                	  currentLexeme += (char)currentCharacter;	
                	}
                	/*throw and exception if a number contains
                	 *anything other than digits.
                	 */
                	else{
                		try {
                            input.unread( currentCharacter);
                        }
                        catch( IOException ioe){
                           
                        }
                        stateNumber = NUMBER_COMPLETE;
                    }
                	break;
                	
                case 3:
                	/*
                	 * go to SYMBOL_COMPLETE if >=,  <= or :=
                	 * go to SHORT_SYMBOL_COMPLETE if >, < or :
                	 */
                    if( currentCharacter == '=') {
                        stateNumber = SYMBOL_COMPLETE;
                        currentLexeme += (char)currentCharacter;                        
                    }
                    else {
                        try {
                            input.unread( currentCharacter);
                        }
                        catch( IOException ioe){
                           
                        }
                        stateNumber = SHORT_SYMBOL_COMPLETE;
                    }
                    break;
                case 4: 
                	/*
                	 * if a { symbol is before a } symbol, go to the error state.
                	 * 
                	 * Since the brackets represent comments,
                	 * ignore anything between { and }
                	 */
                    if( currentCharacter == '{') {
                    	currentLexeme += (char)currentCharacter;
                        stateNumber = ERROR;
                    }
                    else if(currentCharacter == '}') {
                        stateNumber = 0;
                    }
                    else {
                    	
                    }
                    break;
                case 5:
                	// go to symbol complete if you have :=.
                	if( currentCharacter == '='){
                		stateNumber = SYMBOL_COMPLETE;
                		currentLexeme += (char)currentCharacter;
                	}
                	else{
                		try {
                            input.unread( currentCharacter);
                        }
                        catch( IOException ioe){
                           
                        }
                        stateNumber = ERROR;
                	}
                	break;
        } 
        this.lexeme = currentLexeme;
        /*
         * Don't give a token type if it reaches the error state.
         */
        if( stateNumber == ERROR) {
            this.type = null;
            return(false);
        }
        //When a lexeme is complete assign an ID.
        else if( stateNumber == ID_COMPLETE) {
            this.type = lookup.get( this.lexeme);
            System.out.println("lookup [" + this.lexeme + "] found " + this.type);
            if( this.type == null) {
                this.type = EquationToken.ID;
            }
            return(true);
        }
        else if( stateNumber == NUMBER_COMPLETE){
              this.type = EquationToken.NUM;
              return(true);
        }
        else if( stateNumber == SYMBOL_COMPLETE) {
            this.type = lookup.get( this.lexeme);
            return(true);
        }
        else if( stateNumber == SHORT_SYMBOL_COMPLETE) {
            this.type = lookup.get( this.lexeme);
            return(true);
        }
        }
        return(false);
    }
    public EquationToken getToken() { return this.type;}
    
    public String getLexeme() { return this.lexeme;}   
}