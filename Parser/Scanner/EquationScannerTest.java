package Parser;

import java.io.File;
import Parser.EquationScanner;

public class EquationScannerTest {
	
	 public static void main(String[] args) {
		 	/*
		 	 * Creates a new scanner with the test file as input.
		 	 */
	        EquationScanner s = new EquationScanner( new File( "C:/Test/basicPascal.txt"));
	        
	        /*
	         * If there is another token the scanner will try to find the next one.
	         */
	        boolean thereIsAToken = true;
	        while( thereIsAToken) {
	            thereIsAToken = s.nextToken();
	            
	            /*
	             * If there is a token it will be stated with it's matching lexeme.
	             * If there is not token, the console will say so and list the lexeme.
	             */
	            if( thereIsAToken) {
	                System.out.println("Found " + s.getToken() + " with lexeme " + s.getLexeme());
	            }
	            else {
	                System.out.println("Didn't find token with lexeme " + s.getLexeme());
	            }
	        }
	        
	    }
}
