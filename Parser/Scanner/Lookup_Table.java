package Parser;
import java.util.Hashtable;

/*
 * This table matches each keyword and symbol
 * up with a token type.
 */

public class Lookup_Table extends Hashtable<String, EquationToken> {
	
	public Lookup_Table(){
		super();
		this.put("var", EquationToken.VARIABLE);
		this.put("array", EquationToken.ARRAY);
		this.put("num", EquationToken.NUM);
		this.put("of", EquationToken.OF);
		this.put("or", EquationToken.OR);
		this.put("integer", EquationToken.INTEGER);
		this.put("real", EquationToken.REAL);
		this.put("function", EquationToken.FUNCTION);
		this.put("procedure", EquationToken.PROCEDURE);
		this.put("begin", EquationToken.BEGIN);
		this.put("end", EquationToken.END);
		this.put("if", EquationToken.IF);
		this.put("then", EquationToken.THEN);
		this.put("else", EquationToken.ELSE);
		this.put("while", EquationToken.WHILE);
		this.put("do", EquationToken.DO);
		this.put("not", EquationToken.NOT);
		this.put("div", EquationToken.DIV);
		this.put("mod", EquationToken.MOD);
		this.put("and", EquationToken.AND);
		this.put("{", EquationToken.LEFT_BRACKET);
		this.put("}", EquationToken.RIGHT_BRACKET);
		this.put("=", EquationToken.EQUALS);
		this.put("<", EquationToken.LESS_THAN);
		this.put(">", EquationToken.GREATER_THAN);
		this.put("<=", EquationToken.LESS_THAN_OR_EQUAL);
		this.put(">=", EquationToken.GREATER_THAN_OR_EQUAL);
		this.put("+", EquationToken.ADDOP);
		this.put("-", EquationToken.MINUS);
		this.put("*", EquationToken.MULOP);
		this.put("/", EquationToken.DIVIDE);
		this.put(".", EquationToken.DOT);
		this.put(":=", EquationToken.ASSIGNOP);
		this.put("(", EquationToken.LEFT_PAREN);
		this.put(")", EquationToken.RIGHT_PAREN);
		this.put("[", EquationToken.LEFT_BRACE);
		this.put("]", EquationToken.RIGHT_BRACE);
		this.put("program", EquationToken.PROGRAM);
		this.put("ID", EquationToken.ID);
		this.put(";", EquationToken.SEMI_COLON);
		this.put(":", EquationToken.COLON);
		this.put(",", EquationToken.COMMA);
	}
}
