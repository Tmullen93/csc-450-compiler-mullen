package ProjectPart3;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SymbolTable {
private Hashtable<String, IdInfo> symbols = new Hashtable<String, IdInfo>();
	/*
	 * Add a new object to the hash table.
	 * storing it's name and kind
	 */
	public boolean add (String name, IdKind kind){
		if(symbols.get(name) !=null) return false;
		IdInfo ii = new IdInfo(name, kind);
		ii.ident = name;
		ii.kind = kind;
		symbols.put(name, ii);
		return true;
	}
	/*
	 * Check to see if the new object is a variable.
	 */
	public boolean isVariableName(String name){
		IdInfo a = symbols.get(name);
		if(a.kind == IdKind.VARIABLE) return true;
		return false;
	}
	
	/*
	 * Check to see if the new object is a function.
	 */
	public boolean isFunctionName(String name){
		IdInfo a = symbols.get(name);
		if(a.kind == IdKind.FUNCTION) return true;
		return false;
	}
	
	/*
	 * Check to see if the new object is a program.
	 */
	public boolean isProgram(String name){
			IdInfo a = symbols.get(name);
			if(a.kind == IdKind.PROGRAM) return true;
			return false;
		}
	
	/*
	 * Check to see if the new object is an array.
	 */
	public boolean isArrayName(String name){
		IdInfo a = symbols.get(name);
		if(a.kind == IdKind.ARRAY) return true;
		return false;
	}
	/*
	 * @see java.lang.Object#toString()
	 * use the toString function to print our symbol table neatly.
	 * 
	 * There is also an iterator alphabetize the table.
	 */
	public String toString(){
		String returnString = "";
		Set<String> keys = symbols.keySet();
		TreeSet<String> sorted = new TreeSet<String>();
		for(String key: keys){
			sorted.add("value of " + key + " is: " + symbols.get(key).toString() + "\n");
		}
		Iterator<String> iterator = sorted.iterator();
		while(iterator.hasNext()) {
			returnString += iterator.next();
		}
		return returnString;
	}

	public IdKind kind(String name){
		IdInfo ii = symbols.get(name);
		if(ii == null) return null;
		return ii.kind;
	}
	/*
	 * Get the IDInfo of an object
	 * and return it's kind to the toString.
	 */
	private class IdInfo{
		String ident;
		IdKind kind;
		
		IdInfo(String ident, IdKind kind){
			this.ident = ident;
			this.kind = kind;
		}
		
		public String toString() {
			switch (kind) {
			case PROGRAM : return "program";
			case ARRAY : return "array";
			case FUNCTION : return "function";
			case VARIABLE : return "variable";
			default : return "error";
			}
		}
	}
	
	public enum IdKind{
		PROGRAM,
		ARRAY,
		FUNCTION,
		VARIABLE;
	}
}
