package ProjectPart3;
import static org.junit.Assert.*;
import org.junit.Test;

public class SymbolTableTest {
/*
 * These tests create a symbol table and use assertSame
 * to make sure that the functions work properly.
 * and that all of the values are stored.
 */
	/**
	 * Test method for {@link SymbolTable#add(java.lang.String, SymbolTable.IdKind)}.
	 */
	@Test
	public void testAdd() {
		SymbolTable george = new SymbolTable();
		boolean expected = true;
		boolean actual = george.add("bob", SymbolTable.IdKind.VARIABLE);
		assertSame(expected, actual);
		
		expected = false;
		actual = george.add("bob", SymbolTable.IdKind.VARIABLE);
		assertSame(expected, actual);
		
		expected = true;
		actual = george.add("fred", SymbolTable.IdKind.VARIABLE);
		assertSame(expected, actual);
		
		expected = true;
		actual = george.add("array", SymbolTable.IdKind.ARRAY);
		assertSame(expected, actual);
		
		expected = false;
		actual = george.add("array", SymbolTable.IdKind.ARRAY);
		assertSame(expected, actual);
		
		expected = false;
		actual = george.add("fred", SymbolTable.IdKind.VARIABLE);
		assertSame(expected, actual);
		
		expected = true;
		actual = george.add("null", SymbolTable.IdKind.VARIABLE);
		assertSame(expected, actual);
		
		expected = false;
		actual = george.add("bob", SymbolTable.IdKind.FUNCTION);
		assertSame(expected, actual);
		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link SymbolTable#isVariableName(java.lang.String)}.
	 */
	@Test
	public void testIsVariableName() {
		SymbolTable george = new SymbolTable();
		george.add("fred", SymbolTable.IdKind.VARIABLE);
		george.add("bob", SymbolTable.IdKind.VARIABLE);
		george.add("fum", SymbolTable.IdKind.FUNCTION);
		george.add("set", SymbolTable.IdKind.ARRAY);
		george.add("program", SymbolTable.IdKind.PROGRAM);
		george.add("table", SymbolTable.IdKind.ARRAY);
		george.add("47", SymbolTable.IdKind.VARIABLE);
		george.add("do", SymbolTable.IdKind.FUNCTION);
		
		boolean expected = true;
		boolean actual = george.isVariableName("fred");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isVariableName("bob");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isVariableName("fum");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isVariableName("set");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isVariableName("program");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isVariableName("table");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isVariableName("47");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isVariableName("do");
		assertSame(expected, actual);
		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link SymbolTable#isFunctionName(java.lang.String)}.
	 */
	@Test
	public void testIsFunctionName() {
		SymbolTable george = new SymbolTable();
		george.add("fred", SymbolTable.IdKind.VARIABLE);
		george.add("bob", SymbolTable.IdKind.VARIABLE);
		george.add("fum", SymbolTable.IdKind.FUNCTION);
		george.add("set", SymbolTable.IdKind.ARRAY);
		george.add("program", SymbolTable.IdKind.PROGRAM);
		george.add("table", SymbolTable.IdKind.ARRAY);
		george.add("47", SymbolTable.IdKind.VARIABLE);
		george.add("do", SymbolTable.IdKind.FUNCTION);
		
		boolean expected = false;
		boolean actual = george.isFunctionName("fred");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isFunctionName("bob");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isFunctionName("fum");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isFunctionName("set");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isFunctionName("program");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isFunctionName("table");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isFunctionName("47");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isFunctionName("do");
		assertSame(expected, actual);
		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link SymbolTable#isProgram(java.lang.String)}.
	 */
	@Test
	public void testIsProgram() {
		SymbolTable george = new SymbolTable();
		george.add("fred", SymbolTable.IdKind.VARIABLE);
		george.add("bob", SymbolTable.IdKind.VARIABLE);
		george.add("fum", SymbolTable.IdKind.FUNCTION);
		george.add("set", SymbolTable.IdKind.ARRAY);
		george.add("program", SymbolTable.IdKind.PROGRAM);
		george.add("table", SymbolTable.IdKind.ARRAY);
		george.add("47", SymbolTable.IdKind.VARIABLE);
		george.add("do", SymbolTable.IdKind.FUNCTION);
		
		boolean expected = false;
		boolean actual = george.isProgram("fred");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isProgram("bob");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isProgram("fum");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isProgram("set");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isProgram("program");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isProgram("table");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isProgram("47");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isProgram("do");
		assertSame(expected, actual);
		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link SymbolTable#isArrayName(java.lang.String)}.
	 */
	@Test
	public void testIsArrayName() {
		SymbolTable george = new SymbolTable();
		george.add("fred", SymbolTable.IdKind.VARIABLE);
		george.add("bob", SymbolTable.IdKind.VARIABLE);
		george.add("fum", SymbolTable.IdKind.FUNCTION);
		george.add("set", SymbolTable.IdKind.ARRAY);
		george.add("program", SymbolTable.IdKind.PROGRAM);
		george.add("table", SymbolTable.IdKind.ARRAY);
		george.add("47", SymbolTable.IdKind.VARIABLE);
		george.add("do", SymbolTable.IdKind.FUNCTION);
		
		boolean expected = false;
		boolean actual = george.isArrayName("fred");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isArrayName("bob");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isArrayName("fum");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isArrayName("set");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isArrayName("program");
		assertSame(expected, actual);
		
		expected = true;
		actual = george.isArrayName("table");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isArrayName("47");
		assertSame(expected, actual);
		
		expected = false;
		actual = george.isArrayName("do");
		assertSame(expected, actual);
		//fail("Not yet implemented"); // TODO
	}
	
	@Test
	/*
	 * The entries in the symbol table should
	 * come out in alphabetical order.
	 * 
	 * Symbol tables are created and tested using assertSame.
	 */
	public void testToString(){
		SymbolTable george = new SymbolTable();
		george.add("fred", SymbolTable.IdKind.VARIABLE);
		george.add("bob", SymbolTable.IdKind.VARIABLE);
		george.add("fum", SymbolTable.IdKind.FUNCTION);
		george.add("set", SymbolTable.IdKind.ARRAY);
		george.add("program", SymbolTable.IdKind.PROGRAM);
		george.add("table", SymbolTable.IdKind.ARRAY);
		george.add("47", SymbolTable.IdKind.VARIABLE);
		george.add("do", SymbolTable.IdKind.FUNCTION);

		System.out.println(george.toString());
		
		String expected = "value of 47 is: variable\n"
		+ "value of bob is: variable\n"
		+ "value of do is: function\n"
		+ "value of fred is: variable\n"
		+ "value of fum is: function\n"
		+ "value of program is: program\n"
		+ "value of set is: array\n"
		+ "value of table is: array\n";
		
		String actual = george.toString();
		assertEquals(expected, actual);
		
		
		SymbolTable foo = new SymbolTable();
		foo.add("fi", SymbolTable.IdKind.VARIABLE);
		foo.add("set", SymbolTable.IdKind.ARRAY);
		foo.add("make", SymbolTable.IdKind.FUNCTION);
		foo.add("run", SymbolTable.IdKind.FUNCTION);
		foo.add("program", SymbolTable.IdKind.PROGRAM);
		
		System.out.println(foo.toString());
		
		expected = "value of fi is: variable\n"
		+ "value of make is: function\n"
		+ "value of program is: program\n"
		+ "value of run is: function\n"
		+ "value of set is: array\n";
		
		actual = foo.toString();
		assertEquals(expected, actual);
		
		
		SymbolTable hash = new SymbolTable();
		hash.add("program", SymbolTable.IdKind.PROGRAM);
		hash.add("code", SymbolTable.IdKind.VARIABLE);
		hash.add("level", SymbolTable.IdKind.VARIABLE);
		hash.add("go", SymbolTable.IdKind.FUNCTION);
		hash.add("block", SymbolTable.IdKind.ARRAY);
		
		System.out.println(hash.toString());
		
		expected = "value of block is: array\n"
		+ "value of code is: variable\n"
		+ "value of go is: function\n"
		+ "value of level is: variable\n"
		+ "value of program is: program\n";
		
		actual = hash.toString();
		assertEquals(expected, actual);
	}
}
